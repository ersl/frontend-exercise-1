# ERS Frontend Developer
### Exercise 1

At ERS, we implement corporate registries. Implementing a corporate registry involves developing forms, lots of forms! To speed up development time and performance we've decided to use Angular 2 in our frontend application. To demonstrate your ability to use Angular 2, you'll be creating a new project and implementing a typical business form. 

#### Tasks:

1. Create a new Angular 2 project. 
	* Create a branch with the name: frontend-exercise-1-yourfirstname-yoursurname
	* Use Angular CLI to create a new project.
	* Add Moment.js v2.13.0 as a project dependency.

2. Implement the RF805 form
	* See RF805.pdf. This is the paper version of the form you will implement. 
	* Your form must contain all fillable fields found in RF805.pdf, with some exceptions:
		- You are not required to include the '3. Applicant' section found in the pdf.
		- You are not required to include the 'Registry Use Only' section found in the pdf.
	* Your solution does not require the 'Guidance Notes' in the pdf.
	* The 'Requested Mark' section should be 3 text boxes for 'Manufacturer and Manufacturer's Designation of Aircraft', 'Manufacturer Serial Number (MSN)' and 'Requested Registration Mark' with the ability to add additional rows, allowing the user to request multiple marks.

3. Rules
	* All fields are required with the exception of:
		- '2. Applicant - Fax'
		- '2. Applicant - Mobile'
	* The 'Requested Registration Mark must not contain the letters 'I', 'O' or 'S'.
	* 'Email address' must be a valid email address

4. Implement a mock service called 'submission'
	* Your form must submit to this service.
	* The service must return a submission ID number (this can be a random number).
	* You must indicate to the user that the form has been submitted successfully and present the submission ID number.

5. Once you've completed the exercise, push your branch with the following name: frontend-exercise-1-yourfirstname-yoursurname.

Should you have any questions, please feel free to contact Niall using the details below.

Created by Niall McKiernan - [ERS](http://ersl.ie) / Email: [nmk@ersl.ie](mailto:nmk@ersl.ie) / Skype: ersnmk